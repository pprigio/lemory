
from django.contrib import admin

# Register your models here.
from lemory_core.models import Context
from lemory_core.models import Book
from lemory_core.models import BookSection
from lemory_core.models import TermImage
from lemory_core.models import KeyTerm
from lemory_core.models import LangWord
from lemory_core.models import TestDefinition
from lemory_core.models import TestDefinitionWords
from lemory_core.models import TestRun
from lemory_core.models import TestRunWords
from lemory_core.models import StudentClass


class LangWordAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Refers to',               {'fields': ['term', 'lang']}),
        ('Definition', {'fields': ['genre','sart','sword','part','pword'],}),
        ('More', {'fields': ['pronounce_file','example'],}), # 'classes': ['collapse']}),
    ]
    list_display = ('sword','sart','lang')
    list_filter = ['lang']
    
class LangWordInline(admin.TabularInline):
    model = LangWord
    extra = 3

class KeyTermAdmin(admin.ModelAdmin):
    #fieldsets = [
    #    ('Refers to',               {'fields': ['word', 'lang']}),
    #    ('Information', {'fields': ['genre','sart','sword','part','pword'],}), # 'classes': ['collapse']}),
    #]
    inlines = [LangWordInline]
    list_display = ('name', 'type')
    list_filter = ['contexts']
    
class TestDefinitionAdmin(admin.ModelAdmin):
    fieldsets = [
        ('Definition',               {'fields': ['name', 'level', 'lang','valid_from','allow_review']}),
        ('Contents', {'fields': ['contexts','book_sections',],}),
    ]
    list_display = ('name', 'level', 'lang')
    list_filter = ['name', 'level', 'lang']
  
 
class TermImageAdmin(admin.ModelAdmin):
    prepopulated_fields = {"slug": ("title",)}
  
admin.site.register(Context)
admin.site.register(Book)
admin.site.register(BookSection)
admin.site.register(TermImage,TermImageAdmin)
admin.site.register(KeyTerm, KeyTermAdmin)
admin.site.register(LangWord,LangWordAdmin)
admin.site.register(TestDefinition,TestDefinitionAdmin)
admin.site.register(TestDefinitionWords)
admin.site.register(TestRun)
#admin.site.register(TestRunWords)
admin.site.register(StudentClass)

