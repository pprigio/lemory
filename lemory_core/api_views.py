from django.http import HttpResponse, Http404
from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
import json

# Create your views here.
from lemory_core.models import LangWord, TestDefinition, TestDefinitionWords, TestRun, TestRunWords
import logging
logger = logging.getLogger('tracker')

@login_required
def test_words(request,test_id,test_run_id):
    try:
        word_list=TestDefinitionWords.objects.filter(test_definition=test_id)
        words = []
        for wl in word_list:
            w = {}
            w['order'] = wl.order
            w['random_id'] = wl.rand_id
            w['lang'] = wl.lang_word.lang
            w['genre'] = wl.lang_word.genre
            if len(wl.lang_word.sword): 
                w['has_singular'] = "y" 
            else: 
                w['has_singular'] = "n"
            if len(wl.lang_word.pword): 
                w['has_plural'] = "y" 
            else: 
                w['has_plural'] = "n"
            w['img_url'] = wl.lang_word.term.image.source 
            w['img_author'] = wl.lang_word.term.image.author
            words.append(w)
    except TestDefinitionWords.DoesNotExist:
        raise Http404
    return HttpResponse(json.dumps(words),content_type="application/json")
    
@login_required
def test_word_verify(request,test_id,test_run_id,test_word_id):
    if not request.POST.has_key('singular') and not request.POST.has_key('plural'):
        return HttpResponse(status=422,content_type="application/json")

    try:
        test_run = TestRun.objects.get(id=test_run_id, test_definition=test_id, performed_by=request.user.id, test_definition__accessible_to_classes__students__id=request.user.id)
    except TestRun.DoesNotExist:
        raise Http404
    try:
        test_def_word = TestDefinitionWords.objects.filter(test_definition=test_id, test_definition__accessible_to_classes__students__id=request.user.id, order=test_word_id)[0]
        lang_word = test_def_word.lang_word
    except:
        raise Http404

    try:
        test_run_word = TestRunWords.objects.get(test_run=test_run_id, test_run__performed_by = request.user.id, test_run__status = TestRun.STATUSES_RUNNING, test_word = test_def_word)
    except TestRunWords.DoesNotExist:
        raise Http404

    response_content = {'result_bool' : '', 'details': ''}


    test_ok = False

    tracking_text = 'submission="%s" subm_number=%s subm_result=%s'

    if request.POST.has_key('singular'):
        test = request.POST['singular'].strip()
        test_run_word.sing_tries = test_run_word.sing_tries + 1
        test_run_word.sing_last_entry = test
        if test == lang_word.sing_repr():         
            test_run_word.sing_is_correct = True
            test_ok = True
            tracking_text = tracking_text % (test,'singular','correct')
        else:
            test_run_word.sing_is_correct = False
            tracking_text = tracking_text % (test,'singular','wrong')
        test_run_word.save()
    if request.POST.has_key('plural'):
        test = request.POST['plural'].strip()
        test_run_word.plur_tries = test_run_word.plur_tries + 1
        test_run_word.plur_last_entry = test
        if test == lang_word.plur_repr():
            test_run_word.plur_is_correct = True
            test_ok = True
            tracking_text = tracking_text % (test,'plural','correct')
        else:
            test_run_word.plur_is_correct = False
            tracking_text = tracking_text % (test,'plural','wrong')
        test_run_word.save()

    response_content['result_bool'] = test_ok
    log_msg = "%s; %s; student=%s %s " %(test_run.log_repr(),lang_word.log_repr(),request.user,tracking_text)
    logger.info(log_msg)
    #response_content['valid'] = 'true' # needed by bootstrap-validator.js
    return HttpResponse(json.dumps(response_content),content_type="application/json")
        
