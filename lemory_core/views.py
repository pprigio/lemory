from django.http import HttpResponse, Http404, HttpResponsePermanentRedirect
from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.views.decorators.cache import never_cache, cache_control
from django.template import RequestContext
from django.core.urlresolvers import reverse
import datetime
import json

# Create your views here.
from lemory_core.models import LangWord, TestDefinition, TestDefinitionWords, TestRun, TestRunWords


def index(request):
    #return HttpResponse("Hello, world. You're at the poll index.")
    try:
        words_list = LangWord.objects.order_by('sword')
    except LangWord.DoesNotExist:
        raise Http404
    context = {
        'words_list': words_list,
    }
    return render(request,'lemory_core/index.html',context,context_instance=RequestContext(request))

def credits(request):
    return render(request,'lemory_core/credits.html',{},context_instance=RequestContext(request))


@login_required
def tests_list(request):
    try:
        #tests = TestDefinition.objects.filter(accessible_to_classes__students__id=request.user).order_by('lang')
        tests = TestDefinition.objects.filter(accessible_to_classes__students__id=request.user.id, valid_from__lte=datetime.date.today()).order_by('lang')
    except TestDefinition.DoesNotExist:
        raise Http404
    #Words that need reharsal!
    # lang_words = LangWord.objects.filter(testdefinitionwords__testrunwords__test_run__performed_by=request.user.id)
    #        .annotate(tot_sing_tries = Sum('testdefinitionwords__testrunwords__sing_tries'),tot_plur_tries = Sum('testdefinitionwords__testrunwords__plur_tries')).order_by('-sing_tries')
    
    context = {
        'tests': tests,
    }
    return render(request, 'lemory_core/tests_list.html', context, context_instance=RequestContext(request))

@login_required   
def test_details(request,test_id):
    try:
        #test = TestDefinition.objects.get(pk=test_id,accessible_to_classes__students__id=request.user,valid_from__gte=datetime.date.today())
        test = TestDefinition.objects.get(pk=test_id, accessible_to_classes__students__id=request.user.id, valid_from__lte=datetime.date.today())
        #runs = TestRun.objects.filter(test_definition=test,performed_by=request.user).order_by('date_last_performed')
        runs = TestRun.objects.filter(test_definition=test,performed_by=request.user.id).order_by('-date_last_performed')
        words_count = TestDefinitionWords.objects.filter(test_definition=test).count()
        
        # Update test run statistics for tests that are not marked as completed
        try:
            for test_run in runs:
                if test_run.status != TestRun.STATUSES_COMPLETED:
                    update_test_run_score(test_run)
        except TestRunWords.DoesNotExist:
            raise Http404
            
    except TestDefinition.DoesNotExist:
        raise Http404
    except TestRun.DoesNotExist:
        runs = []
    context = {
        'test': test,
        'runs': runs,
        'words_count': words_count
    }
    return render(request,'lemory_core/test_details.html',context,context_instance=RequestContext(request))
 
@login_required    
def test_runs(request,test_id):
    try:
        test = TestDefinition.objects.filter(pk=test_id, accessible_to_classes__students__id=request.user.id, valid_from__lte=datetime.date.today())[0]
    except TestDefinition.DoesNotExist:
        raise Http404
    
    if request.method == 'POST':
        tr = TestRun(status="new", test_definition=test, performed_by=request.user)
        tr.save()
        max_score = 0
        try:
            lang_words = LangWord.objects.filter(testdefinition = tr.test_definition)
            for lw in lang_words:
                max_score = max_score + bool(lw.sword) + bool(lw.pword)
            tr.max_possible_score = max_score
            tr.save()
        except LangWord.DoesNotExist:
            pass
        try:
            first_word = TestDefinitionWords.objects.filter(test_definition=test_id).order_by('order')[0:1].get()
            return redirect('test execute', test_id=test.id, test_run_id=tr.id,  execution_mode=TestDefinition.EXECUTION_MODE_RUN, test_word=first_word.order)
        except TestDefinitionWords.DoesNotExist:
            return redirect('test_run', test_id=test.id, test_run=tr.id)
    elif request.method == 'GET':
        context = {
            'test': test,
        }
        return render(request,'lemory_core/tests_list.html',context,context_instance=RequestContext(request))
# internal method
# Update test completion statistics
def update_test_run_score(test_run):
    try:
        test_run_words = TestRunWords.objects.filter(test_run=test_run.id)
        count_correct = 0
        count_wrong = 0
        count_empty = 0
        for trw in test_run_words:
            if trw.has_sing:
                if trw.sing_is_correct:
                    count_correct = count_correct + 1
                elif trw.sing_is_correct == False:
                    count_wrong = count_wrong + 1
                else:
                    count_empty = count_empty + 1
            if trw.has_plur:
                if trw.plur_is_correct:
                    count_correct = count_correct + 1
                elif trw.plur_is_correct == False:
                    count_wrong = count_wrong + 1
                else:
                    count_empty = count_empty + 1
        test_run.status = TestRun.STATUSES_COMPLETED
        test_run.score_correct = count_correct
        test_run.score_wrong = count_wrong
        test_run.score_empty = count_empty
        test_run.save()
    except TestRunWords.DoesNotExist:
        raise Http404

@login_required   
def test_run(request,test_id,test_run_id):
    try:
        test_run = TestRun.objects.filter(pk=test_run_id, test_definition=test_id, performed_by=request.user, test_definition__accessible_to_classes__students__id=request.user.id)[0]
    except TestRun.DoesNotExist:
        raise Http404
    test_run_words = None
    if request.method == 'GET' and test_run.status != TestRun.STATUSES_COMPLETED:
        # Update test completion statistics
        try:
            update_test_run_score(test_run)
        except TestRunWords.DoesNotExist:
            raise Http404
    context = {
        'test_run': test_run,
        'test_run_words' : test_run_words,
    }
    return render(request,'lemory_core/test_run_details.html',context,context_instance=RequestContext(request))


@cache_control(must_revalidate=True, no_store = True, no_cache = True)
@login_required 
def word_exercise(request, test_id, test_run_id=-1, test_word=-1, execution_mode="run"):
    # execution modes = run | preview | review
    # test_run_id = 0 only acceptable for preview
    # test_word = 0 means, display the first one
    try:
        test_id = int(test_id)
        test_run_id = int(test_run_id)
        test_word = int(test_word)
    except:
        raise Http404

    if test_run_id == 0 and execution_mode != TestDefinition.EXECUTION_MODE_PREVIEW:
        raise Http404
    elif test_run_id > 0 and execution_mode not in (TestDefinition.EXECUTION_MODE_RUN, TestDefinition.EXECUTION_MODE_REVIEW):
        raise Http404

    test_run = None
    if test_run_id > 0:
        try:
            test_run = TestRun.objects.get(pk=test_run_id, test_definition=test_id, performed_by=request.user, test_definition__accessible_to_classes__students__id=request.user.id)
            if test_run.status == TestRun.STATUSES_NEW:
                test_run.status = TestRun.STATUSES_RUNNING
                test_run.save()
            elif test_run.status == TestRun.STATUSES_COMPLETED and execution_mode == TestDefinition.EXECUTION_MODE_RUN:
                # it is not possible to update a test once completed!
                return HttpResponsePermanentRedirect(reverse('test execute', kwargs={'test_id':test_id, 'test_run_id':test_run_id,  'execution_mode':TestDefinition.EXECUTION_MODE_REVIEW, 'test_word':test_word}))
        except TestRun.DoesNotExist:
            raise Http404
    try:
        if test_word >= 0:
            #retrieve current word and the next one
            word_list = TestDefinitionWords.objects.filter(test_definition=test_id,order__gte=test_word)[0:2]
        else: # invoked without the id of the test word, as when starting a run or preview
            word_list = TestDefinitionWords.objects.filter(test_definition=test_id)[0:2]

        current_definition = word_list[0]
        if len(word_list)>1:
            next_definition = word_list[1]
        else:
            next_definition = None
    except TestDefinitionWords.DoesNotExist:
        raise Http404
    except LangWord.DoesNotExist:
        raise Http404
    
    pf_url = ""
    if current_definition.lang_word.pronounce_file:
        pf_url = current_definition.lang_word.pronounce_file.url
    
    if execution_mode in (TestDefinition.EXECUTION_MODE_RUN,TestDefinition.EXECUTION_MODE_REVIEW):
        try:
            test_run_word = TestRunWords.objects.get(test_run=test_run_id,test_word=current_definition)
        except TestRunWords.DoesNotExist:
            tr = TestRun.objects.get(pk=test_run_id)
            has_sing = current_definition.lang_word.sword != ""
            has_plur = current_definition.lang_word.pword != ""    
            test_run_word = TestRunWords(test_run=tr,test_word=current_definition,has_sing=has_sing, has_plur=has_plur)
            test_run_word.save()
    else:
        test_run_word = None

    context = {
        'execution_mode': execution_mode,
        'test_id': test_id,
        'test_run_id': test_run_id,
        'test_run_word': test_run_word,
        'current_definition': current_definition,
        'image_display_method' : word_list[0].lang_word.term.image.getDisplayMethod(), # "file","url","embed"
        'next_definition' : next_definition,
        'pronounce_file_url' : pf_url,
    }
    return render(request,'lemory_core/word_test.html',context,context_instance=RequestContext(request))
    



