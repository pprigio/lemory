# coding: UTF-8
# Create your models here.
from django.db import models
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator
from django.utils.text import slugify

# https://docs.djangoproject.com/en/1.6/topics/db/models/

AVAILABLE_LANGUAGES = (
    ("en","inglese"),
    ("fr","francese"),
    ("de","tedesco"),
    ("it","italiano"),
)

def storagePathLangWord(instance, filename):
    if instance.lang != "":
        return 'pronounce/%s/%s/%s' % (instance.lang, filename[0], filename)
    else:
        return 'pronounce/%s/%s' % (filename[0],filename)

def storagePathTermImage(instance, filename):
    ext = filename.split(".")[-1]
    return 'image/%s/%s.%s' % (instance.slug[0], instance.slug, ext)
    
        
class Context(models.Model):
    name = models.CharField(max_length=100)
    parent_context = models.ForeignKey('Context', blank=True,null=True,related_name="sub_context")
    descr = models.CharField("description", max_length=200,blank=True)
    def __unicode__(self):
        return self.name
    class Meta:
        ordering = ["name"]
        verbose_name_plural = "contexts"

class Book(models.Model):
    title = models.CharField(max_length=100)
    lang = models.CharField("language",max_length=2,choices=AVAILABLE_LANGUAGES)
    level = models.CharField(max_length=10)
    
    def __unicode__(self):
        return self.title + " (" + self.lang + ")"
    class Meta:
        ordering = ["lang","title"]
        verbose_name_plural = "books"
        
class BookSection(models.Model):
    book = models.ForeignKey(Book)
    order = models.PositiveIntegerField(blank=True)
    name = models.CharField(max_length=100)
    contexts = models.ManyToManyField(Context,blank=True,related_name="+")
    def __unicode__(self):
        return self.book.title + " - " + self.name
    class Meta:
        ordering = ["order"]
        order_with_respect_to = "book"
        verbose_name_plural = "sections"

class TermImage(models.Model):
    title = models.CharField(max_length=50)
    slug = models.SlugField(max_length=70,unique=True)
    file = models.ImageField(upload_to=storagePathTermImage,blank=True)
    url = models.URLField(blank=True)
    embed_html = models.CharField(max_length=1000,blank=True)
    copyright_notice = models.CharField(max_length=100,blank=True)
    copyright_url = models.CharField(max_length=100,blank=True)
    keywords = models.CharField("List of keywords",max_length=50,blank=True,null=True)
    def __unicode__(self):
        return self.slug
    class Meta:
        verbose_name_plural = "images"
    
    def save(self, *args, **kwargs):    # autocomplete the slug
        if self.title:
            self.slug = slugify(self.title)
        super(TermImage, self).save(*args, **kwargs)

    def getDisplayMethod(self): # What is the correct way to display this image?
        if self.file or self.url:
            return 'url'
        if self.embed_html:
            return 'embed'
    
    def getImageUrl(self):     # Which is the correct URL?
        if self.file:
            return self.file.url
        elif self.url:
            return self.url
        else:
            return None

class KeyTerm(models.Model):
    TERM_TYPES = (
        ("adjective","adjective"),
        ("noun","noun"),
        ("preposition","preposition"),
        ("verb","verb"),
    )
    name = models.CharField(max_length=50)
    image = models.ForeignKey(TermImage,blank=True,null=True)
    type = models.CharField(max_length=20,choices=TERM_TYPES)
    contexts = models.ManyToManyField(Context,blank=True)
    book_sections = models.ManyToManyField(BookSection,blank=True)
    synonims = models.ManyToManyField('KeyTerm',blank=True,null=True,related_name="synonim")
    opposites = models.ManyToManyField('KeyTerm',blank=True,null=True,related_name="opposite")
    
    def __unicode__(self):
        return self.name + "(" + self.type +")"
    class Meta:
        ordering = ["name"]
        verbose_name_plural = "key-terms"


class LangWord(models.Model):
    GENRES = (
        ('m','m'),
        ('f','f'),
        ('n','n'),
    )
    term = models.ForeignKey(KeyTerm)
    lang = models.CharField("language",max_length=2,choices=AVAILABLE_LANGUAGES)
    genre = models.CharField(max_length=1,choices=GENRES,blank=True)
    sart = models.CharField("singular article",max_length=5,blank=True)
    sword = models.CharField("singular word",max_length=50,blank=True)
    part = models.CharField("plural article",max_length=5,blank=True)
    pword = models.CharField("plural word",max_length=50,blank=True)
    pronounce_file = models.FileField(upload_to=storagePathLangWord,blank=True,null=True)
    example = models.CharField("usage example",max_length=100,blank=True)
    def sing_repr(self):
        if self.sart == None or self.sart == "":
            return self.sword
        elif self.sart[-1] in (u"'", u"´", u"`"):
            return self.sart + self.sword
        else:
            return self.sart + " " + self.sword
    def plur_repr(self):
        if self.part == None or self.part == "":
            return self.pword
        elif self.part[-1] in (u"'", u"´", u"`"):
            return self.part + self.pword
        else:
            return self.part + " " + self.pword
    def log_repr(self):
        return 'word="%s" lang="%s" word_type="%s" word_genre="%s"' % (self.__unicode__(), self.lang, self.term.type, self.genre)

    def __unicode__(self):
        if self.sword:
            return self.sing_repr()
        elif self.pword:
            return self.plur_repr()
        
    # https://docs.djangoproject.com/en/1.6/ref/models/instances/#django.db.models.Model.get_absolute_url
    #def get_absolute_url(self):
    #    from django.core.urlresolvers import reverse
    #    return reverse('people.views.details', args=[str(self.id)])
    class Meta:
        ordering = ["lang"]
        order_with_respect_to = "term"
        verbose_name_plural = "in-language words"
        

class TestDefinition(models.Model):
    EXECUTION_MODE_RUN = 'run'
    EXECUTION_MODE_PREVIEW = 'preview'
    EXECUTION_MODE_REVIEW = 'review'
    name = models.CharField(max_length=100)
    level = models.CharField(max_length=100)
    lang = models.CharField("language",max_length=2,choices=AVAILABLE_LANGUAGES)
    contexts = models.ManyToManyField(Context,blank=True,null=True)
    book_sections = models.ManyToManyField(BookSection,blank=True,null=True)
    words = models.ManyToManyField(LangWord, through='TestDefinitionWords',blank=True,null=True)
    num_words = models.PositiveIntegerField(default=0) #number of words the test is made of
    num_checks = models.PositiveIntegerField(default=0) #number of single checks performed (a word with sing and plur counts 2)
    
    allow_review = models.BooleanField(default=True)
    valid_from = models.DateTimeField(blank=True,null=True)
    def __unicode__(self):
        return "%s (%s)" %( self.name, self.lang)
    def save(self, *args, **kwargs):
        wc = 0
        cc = 0
        self.num_words = 0
        self.num_checks = 0
        if self.pk > 0:
            for lw in self.words.all():
                wc = wc + 1
                if lw.sword != "":
                    cc = cc + 1
                if lw.pword != "":
                    cc = cc + 1
        self.num_words = wc
        self.num_checks = cc
        super(TestDefinition, self).save(*args, **kwargs) # Call the "real" save() method.  
    
    class Meta:
        ordering = ["lang","level","name"]
        verbose_name_plural = "test definitions"

class TestDefinitionWords(models.Model):
    test_definition = models.ForeignKey(TestDefinition)
    lang_word = models.ForeignKey(LangWord)
    order = models.PositiveIntegerField(default=0)
    rand_id = models.PositiveIntegerField(default=0)
    
    def __unicode__(self):
        return "Test:%s, \"%s\", %s)" %( self.test_definition, self.lang_word,self.order)
    class Meta:
        ordering = ["order"]
        order_with_respect_to = "test_definition"
        verbose_name_plural = "test definition words"
    def save(self, *args, **kwargs):
        super(TestDefinitionWords, self).save(*args, **kwargs) # Call the "real" save() method.
        if self.test_definition.pk:
            self.test_definition.save()
    def delete(self, *args, **kwargs):
        td = self.test_definition      
        super(TestDefinitionWords, self).save(*args, **kwargs) # Call the "real" save() method.
        ts.save()
        
class TestRun(models.Model):
    STATUSES_NEW = 'new'
    STATUSES_RUNNING = 'running'
    STATUSES_COMPLETED = 'completed'
    STATUSES = (
        (STATUSES_NEW, 'nuovo'),
        (STATUSES_RUNNING, 'in corso'),
        (STATUSES_COMPLETED, 'completato'),
    )
    test_definition = models.ForeignKey(TestDefinition)
    performed_by = models.ForeignKey(User)
    date_created = models.DateTimeField(auto_now_add=True)
    date_last_performed = models.DateTimeField(auto_now=True)
    seq = models.PositiveIntegerField(blank=True)    # represents the order in which the user created the testrun.
    status = models.CharField(max_length=10,choices=STATUSES)
    max_possible_score = models.PositiveIntegerField(default=0) # should be equal to TestDefinition.num_checks, but it is computed at the completion on the run on the final configuration.
    score_correct = models.PositiveIntegerField(default=0)
    score_wrong = models.PositiveIntegerField(default=0)
    score_empty = models.PositiveIntegerField(default=0)
    def save(self, *args, **kwargs):
        # compute the SEQ field, which is unique by performed_by and and test_definition
        if not self.seq and self.test_definition and self.performed_by:
            self.seq = TestRun.objects.filter(test_definition=self.test_definition,performed_by=self.performed_by).count() + 1
        super(TestRun, self).save(*args, **kwargs) # Call the "real" save() method.

    def log_repr(self):
        return 'test="%s" run_seq=%s run_date="%s"' % (self.test_definition.name, self.seq, self.date_created.strftime("%Y-%m-%d %H:%M:%S"))

    def __unicode__(self):
        return "%s by %s on %s" %(self.test_definition, self.performed_by, self.date_created.strftime("%c"))

    class Meta:
        ordering = ["date_created"]
        order_with_respect_to = "test_definition"
        verbose_name_plural = "test executions"
   
class TestRunWords(models.Model):
    #date_created = models.DateTimeField(auto_now_add=True)
    #last_update = models.DateTimeField(auto_now=True)

    test_run = models.ForeignKey(TestRun)
    test_word = models.ForeignKey(TestDefinitionWords)
    
    has_sing = models.BooleanField()
    sing_last_entry = models.CharField(max_length=50,blank=True)
    sing_is_correct = models.NullBooleanField(blank=True)
    sing_tries = models.PositiveIntegerField(default=0)

    has_plur = models.BooleanField()
    plur_last_entry = models.CharField(max_length=50,blank=True)
    plur_is_correct = models.NullBooleanField(blank=True)
    plur_tries = models.PositiveIntegerField(default=0)
    
    def __unicode__(self):
        return "%s in test %s" %(self.test_word,self.test_run)
    class Meta:
        verbose_name_plural = "test-run words"


class StudentClass(models.Model):
    name = models.CharField("class name",max_length=50)
    school = models.CharField(max_length=50,blank=True,null=True)
    year = models.PositiveIntegerField(default=0)
    lang = models.CharField("language",max_length=2,choices=AVAILABLE_LANGUAGES)
    students = models.ManyToManyField(User,related_name="classes")
    available_tests = models.ManyToManyField(TestDefinition,related_name="accessible_to_classes")

    def __unicode__(self):
        return "%s %s" %(self.lang, self.name)
    class Meta:
        ordering = ["lang","school","year"]
        verbose_name_plural = "classes"

