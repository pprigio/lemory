from django.conf.urls import patterns, url

from lemory_core import views, api_views


urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^login$', 'django.contrib.auth.views.login', {'template_name': 'lemory_core/login.html'},name='login'),
    url(r'^login\?next=(?P<next>.+)$', 'django.contrib.auth.views.login', {'template_name': 'lemory_core/login.html'},name='login next'), 
    url(r'^logout$', 'django.contrib.auth.views.logout', {'next_page':'/'}, name='logout'),
    url(r'^credits/?$', views.credits, name='credits'),
    url(r'^tests/$', views.tests_list, name='tests list'),
    url(r'^tests/(?P<test_id>\d+)/?$', views.test_details, name='test details'),
    # only used to create a new test run, redirects to the created run.
    url(r'^tests/(?P<test_id>\d+)/runs/$', views.test_runs, name='test new run'),
    url(r'^tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/?$', views.test_run, name='test run'),
    url(r'^tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/(?P<execution_mode>\w+)/terms/?$',
        views.word_exercise,
        {'test_word':0,},
        name='test execute start'),
    url(r'^tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/(?P<execution_mode>\w+)/terms/(?P<test_word>\d+)/?$',
        views.word_exercise,
        name='test execute'),
#   '''
#   url(r'^tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/terms/?$',
#       views.word_exercise,
#       {'test_word':-1,'execution_mode':'run'},
#       name='word test start'),
#   url(r'^tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/terms/(?P<test_word>\d+)/?$',
#       views.word_exercise,
#       {'execution_mode':'run'},
#       name='word test'),
#
#   url(r'^tests/(?P<test_id>\d+)/preview/?$',
#       views.word_exercise,
#       {'test_word': -1, 'execution_mode':'preview'},
#       name='word preview start',
#       ),
#   url(r'^tests/(?P<test_id>\d+)/preview/(?P<test_word>\d+)/?$',
#       views.word_exercise,
#       {'execution_mode':'preview'},
#       name='word preview',
#       ),
#   ''',
    url(r'^api/tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/terms/(?P<test_word_id>\d+)/verify/?$', api_views.test_word_verify, name='test_word_verify'),
    url(r'^api/tests/(?P<test_id>\d+)/runs/(?P<test_run_id>\d+)/terms/', api_views.test_words, name='api_test_words'),
    
)