from lemory_core.models import Context
from lemory_core.models import Book
from lemory_core.models import BookSection
from lemory_core.models import TermImage
from lemory_core.models import KeyTerm
from lemory_core.models import LangWord
from lemory_core.models import TestDefinition
from lemory_core.models import TestDefinitionWords
from lemory_core.models import TestRun
from lemory_core.models import StudentClass
from django.contrib.auth.models import User
from django.core.files import File
import random 

b = Book(title="Allegro",lang="it",level="A1")
b.save()

c = Context(name="casa",descr="casa")
c.save()

bs = BookSection(book=b,order=1,name="sezione 1")
bs.save()
bs.contexts.add(c)







# name type image copyright_notice keywords
key_terms = [
['chair','noun','http://www.ikea.com/us/en/images/products/kritter-childrens-chair__0096632_PE236603_S4.JPG','IKEA','chair'],
['table','noun','http://img.edilportale.com/products/prodotti-62835-rel72f7e43a1c7d44d58cc21048b3f67f17.jpg','?','table'],
['sofa','noun','http://www.vimercatimeda.it/sites/default/files/styles/fullscreen/public/immagine/4_32.jpg?itok=6KvmWn1J','?','sofa'],
['tv','noun','http://www.nonsprecare.it/wp-content/uploads/2013/01/televisione-640x444.jpg','?','tv television'],
['armchair','noun','http://2.bp.blogspot.com/-ePDpR2DrpY4/T71XNcJ9QlI/AAAAAAAAAIg/O1D1kkwqr6E/s400/Fritz+Hansen_Egg+Chair_Poltrona+Cl%C3%A1ssica_2.jpg','?','armchair'],
['wardrobe','noun','http://www.schrankplaner.de/ecke.jpg','?',''],
['kitchen','noun','http://www.schrankplaner.de/ecke.jpg','?',''],
['over','preposition','http://www.schrankplaner.de/ecke.jpg','?',''],
]

for kt in key_terms:
    ti = TermImage(title=unicode(kt[0]),url=kt[2],copyright_notice=kt[3],keywords=kt[4])
    ti.save()
    
    kt = KeyTerm(name=kt[0],image=ti,type=kt[1])
    kt.save()
    kt.contexts.add(c)
    kt.book_sections.add(bs)

#nam lang genre sart sword part pword pronounce.mp3
it_words = [
['chair','it','f','la','sedia','le','sedie','sedia.mp3'],
['table','it','m','il','tavolo','i','tavoli','tavolo.mp3'],
['sofa','it','m','il','divano','i','divani'],
['tv','it','f','la','televisione','le','televisioni'],
['armchair','it','f','la','poltrona','le','poltrone'],
['wardrobe','it','m','lo','armadio','gli','armadi'],
['over','it','','','sopra','',''],
]


def create_lang_words(lw_list):
    for tw in lw_list:
        related_keyterm = KeyTerm.objects.filter(name=tw[0])[0]
        lw = LangWord(term=related_keyterm,lang=tw[1],genre=tw[2],sart=tw[3],sword=tw[4],part=tw[5],pword=tw[6])
        try:
            if tw[7]:
                with open('raw_media\\%s' % tw[7],'rb') as audiofile:
                    lw.pronounce_file.save(tw[7],File(audiofile))
        except:
            pass
        lw.save()

print "---------------------------"
print "Creating in-language words"

create_lang_words(it_words)

def add_words_to_testdef(testdef,language, term_list):
    i = 0
    for tw in term_list:
        i = i + 1
        related_keyterm = KeyTerm.objects.filter(name=tw[0])
        if len(related_keyterm) > 0:
            lw = LangWord.objects.get(lang=language,term=related_keyterm)
            #lw = LangWord(term=related_keyterm[0],lang=tw[1],genre=tw[2],sart=tw[3],sword=tw[4],part=tw[5],pword=tw[6])
            #lw.save()
            tdlw = TestDefinitionWords(test_definition=testdef,lang_word=lw,order=i)#,rand_id=123)
            tdlw.save()

td1 = TestDefinition(name="Parole di casa",level="A1",lang="it",valid_from="2014-04-01")
td1.save()
td1.contexts.add(c)
td1.book_sections.add(bs)

add_words_to_testdef(td1,'it',it_words)

td2 = TestDefinition(name="Parole di casa (short)",level="A1",lang="it",valid_from="2014-05-01")
td2.save()
td2.contexts.add(c)
td2.book_sections.add(bs)

random.shuffle(it_words)
add_words_to_testdef(td2,'it',it_words[:2])
  
u1 = User.objects.create_user('studit1', 'lennon@thebeatles.com', 'password')
u1.last_name = 'Studente 1'
u1.first_name = 'Luca'
u1.save()
u2 = User.objects.create_user('studit2', 'lennon@thebeatles.com', 'password')
u1.first_name = 'Matteo'
u1.last_name = 'Studente 2'
u1.save()


c1 = StudentClass(school="ER2",lang="it",name="7a")
c1.save()
c1.students.add(u1)
c1.students.add(u2)
c1.available_tests.add(td1)
c1.save()


#tr = TestRun(status="new",test_definition=td,performed_by="pp")
#tr.save()
