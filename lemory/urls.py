from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from lemory_core import views
admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'lemory.views.home', name='home'),
    url(r'^$', views.index, name='home'),
    url(r'^/$', views.index, name='home'),
    url(r'^admin/', include(admin.site.urls)),
    url(r'^lemory/', include('lemory_core.urls')),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
