"""
Django settings for lemory project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.6/ref/settings/
"""

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '0sp*yos7bzbu7p+tg$f#$+$fe2f_jeh4#ch7j8g)(=q30kgs#k'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'bootstrap3',
	'lemory_core',
    'mod_wsgi.server',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)


BOOTSTRAP3 = {
    'base_url': '//netdna.bootstrapcdn.com/bootstrap/3.1.1/',
    'css_url': '//netdna.bootstrapcdn.com/bootswatch/3.1.1/lumen/bootstrap.min.css',
}

ROOT_URLCONF = 'lemory.urls'

WSGI_APPLICATION = 'lemory.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
    }
#    'default': {
#        'ENGINE': 'django.db.backends.postgresql_psycopg2',
#        'NAME': 'lemory',
#        'USER': 'lemory',
##        'PASSWORD': 'lemorypassword',
#        'HOST': '127.0.0.1',
#        'PORT': '5432',
#    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'it-IT'

TIME_ZONE = 'CET'

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR,'static')

TEMPLATE_DIRS = [os.path.join(BASE_DIR, 'templates')]

LOGIN_URL = '/lemory/login'
LOGOUT_URL = '/lemory/logout'
LOGIN_REDIRECT_URL = '/lemory/tests'

#MEDIA_ROOT = 'C:/Users/pprigione/lemory/media_files/'
MEDIA_ROOT = os.path.join(BASE_DIR,'media_files')
MEDIA_URL = '/media/'


LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'regular': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'tracking': {
            'format': '[%(asctime)s] %(message)s',
            'datefmt' : "%Y-%m-%d %H:%M:%S"
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            #'filename': 'C:\\Users\\pprigione\\lemory\\log.log',
            'filename': 'log.log',
            'formatter' : 'regular',
        },
        'splunkstorm_tcp':{
            'level': 'INFO',
            'class': 'logging.handlers.SocketHandler',
            'host':'tcp.zu7n-hgps.data.splunkstorm.com',
            'port':11155,
            'formatter' : 'tracking',
        },
        'splunkstorm_udp':{
            'level': 'INFO',
            'class': 'logging.handlers.SysLogHandler',
            #'host':'udp.zu7n-hgps.data.splunkstorm.com',
            'address': ('udp.zu7n-hgps.data.splunkstorm.com',11155),
            #'port':11155,
            'formatter' : 'tracking',
            'facility': 'user',
        },
        'tracker_file':{
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            #'filename': 'C:\\Users\\pprigione\\lemory\\tracker.log',
            'filename': 'tracker.log',
            'formatter' : 'tracking',
        }
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True,
        },
        'django.request': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'tracker': {
            'handlers': ['splunkstorm_udp','tracker_file'],
            'level': 'DEBUG',
            'propagate': True,
        },

    },
}