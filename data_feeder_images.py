
IMAGES = [
{
    'title':'kitchen (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/175168822?et=WAvGwVy8RS9DF64CK2jd6Q&SeoLinks=off&sig=oQtQ3eS9OkR3dSBdpMl7Qxy-AwvR2fujXeP-Su7SAKA=" width="572" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,kitchen'
},
{
    'title':'living room (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/481431415?et=4cIJAbXDTcpqrf85JBicWw&SeoLinks=off&sig=3nTOjnIoAcFbwXlyQ5pgoCAxOwxqo0KTiW-kC3V3IWY=" width="517" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,living room'
},
{
    'title':'bathroom (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/451006859?et=o7mjY3FyS7pW8ndkTY2HNA&SeoLinks=off&sig=ZDSwkFLxKXfRQ3iWsDMgFeZuRbNuQI_DAL_0P8cGWVM=" width="507" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,bathroom'
},
{
    'title':'kid\'s room (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/155276900?et=OwoWir_0QhlVgMxqze-7Dw&SeoLinks=off&sig=n3NR7Cc1ui8FNZdS4rUZ6wM3OpBTxTUHQvVyGxszaF8=" width="478" height="415" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,small bedroom,kid\'s room'
},
{
    'title':'bedroom (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/153091108?et=z6tChR6QSbdxCMNnRra92A&SeoLinks=off&sig=VXQ2ybny1uZUzgTnLxj7o6rXfaen5LwIvQcD8By1W94=" width="515" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,bedroom'
},
{
    'title':'study room (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/468877583?et=xYdgAPMLTsxxiil_Oh-6rw&SeoLinks=off&sig=IuipPcHvY5uDyvt0yEXDSK2zfGGSOvkFE5vBGRC1Hzw=" width="436" height="449" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,study'
},
{
    'title':'garden (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/165960073?et=uFXBi1h-TJhL9pikSZJkqg&SeoLinks=off&sig=JVrf7BFY0s4Z99z8vjeTd7amfZZ7qPgjJTyJW0tfhMA=" width="502" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,garden'
},
{
    'title':'apartment (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/468160325?et=ycFQHqQyS09KLAINyCIrWQ&SeoLinks=off&sig=ldBLEoUu50qFKkwa8FFsi9pqe6NhAJCnVxpejEA6DL0=" width="493" height="404" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,apartment,flat'
},
{
    'title':'apartment entrance (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/170458593?et=__fCT8ozQtlRErB5jANr9g&SeoLinks=off&sig=ngXlZ3aQQEvIJr9lx0kuj2rp40RP9kYehL16yt6gZ5I=" width="398" height="638" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,entrance,doorway,entryway'
},
{
    'title':'dining room (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/175176134?et=spyEEacYTot1oM0NCE2ZJw&SeoLinks=off&sig=mhKWoJT16zcGf4DGzux2DmI_fzJJiMvuA2D2sS0Na8g=" width="536" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,room,dining'
},
{
    'title':'house (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/165796237?et=ay5kEJMTR7lWJWaTTvWbNA&SeoLinks=off&sig=_am9ao-j7Wejid9aLp3Jxj5R_bqq-4vlrhCUD2yQDwo=" width="398" height="529" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house'
},
{
    'title':'wall (gy)',
    'file':'',
    'url':'',
    'embed_html':'<iframe src="//embed.gettyimages.com/embed/157330117?et=BakQiQZcRC5hjdKCCNlPnQ&SeoLinks=off&sig=KbTi1dxuns2xU006TGKBKxJNzTi4blls9pX1eyQNNuM=" width="509" height="398" scrolling="no" frameborder="0" style="display:inline-block;"></iframe>',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':'house,wall'
},
'''
{
    'title':'(gy)',
    'file':'',
    'url':'',
    'embed_html':'',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':''
},
{
    'title':'(gy)',
    'file':'',
    'url':'',
    'embed_html':'',
    'copyright_notice':'Getty images',
    'copyright_url':'',
    'keywords':''
},
'''
]